/*
 *
 * Copyright 2013 Design Coach Sweden.
 */
package se.designcoach.spelprojekt.view;

import com.jme3.app.SimpleApplication;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Plane;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;
import se.designcoach.spelprojekt.model.Ball;
import se.designcoach.spelprojekt.model.BallSimulation;

/**
 *
 * @author lohnn
 */
public class BallView {

    Camera camera;
    SimpleApplication app;
    BallSimulation ballSim;
    Geometry ball;

    public BallView(SimpleApplication app, BallSimulation ballSimulation) {
        this.camera = new Camera();
        this.app = app;
        this.ballSim = ballSimulation;

        camera.setBoardSize(2, 2);
        createFloor();

        Sphere s = new Sphere(64, 64, ballSim.getBall().getRadius());
        ball = new Geometry("Sphere", s);
        redraw();
        Material mat = app.getAssetManager().loadMaterial("Materials/SphereMat.j3m");
        ball.setMaterial(mat);
        this.app.getRootNode().attachChild(ball);

        //Lighting
        AmbientLight al = new AmbientLight();
        al.setColor(ColorRGBA.White);
        app.getRootNode().addLight(al);

        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(1, 0, -2).normalizeLocal());
        sun.setColor(ColorRGBA.White);
        app.getRootNode().addLight(sun);
    }

    public void update(float tpf) {
        ballSim.update(tpf);
        redraw();
    }

    private void createFloor() {
        float ballRadius = ballSim.getBall().getRadius();
        Vector3f startPos = new Vector3f(-ballRadius, -ballRadius, -ballRadius);
        Vector3f endPos = new Vector3f(
                camera.getBoardSize().x + ballRadius,
                camera.getBoardSize().y + ballRadius,
                startPos.z - .5f);
        Box b = new Box(startPos, endPos);
        Geometry box = new Geometry("Floor", b);
        Material mat = new Material(app.getAssetManager(),
                "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Blue);
        box.setMaterial(mat);
        app.getRootNode().attachChild(box);
    }

    private void redraw() {
        Ball tempBall = ballSim.getBall();
        Vector2f worldCoordinate = camera.getWorldCoordinate(tempBall.getPosition());
        ball.setLocalTranslation(worldCoordinate.x, worldCoordinate.y, 0);
    }
}
