/*
 *
 * Copyright 2013 Design Coach Sweden.
 */
package se.designcoach.spelprojekt.view;

import com.jme3.math.Vector2f;

/**
 *
 * @author lohnn
 */
public class ChessView {

    private boolean blackPlayer = false;
    private int tileSize = 64,
            borderSize = 64,
            tilesX = 8,
            tilesY = 8;
    private Vector2f screenSize = new Vector2f(),
            logicalScreenSize = new Vector2f();

    public ChessView() {
        calculateLogicalScreenSize();
        setUpScreenSize();
    }

    private void setUpScreenSize() {
        screenSize.x = getLogicalScreenSize().x;
        screenSize.y = getLogicalScreenSize().y;
    }

    /**
     * Calculates the logical screen size, run this when tile size is changed.
     */
    private void calculateLogicalScreenSize() {
        logicalScreenSize.x = getTileSize() * getTilesX() + getBorderSize() * 2;
        logicalScreenSize.y = getTileSize() * getTilesY() + getBorderSize() * 2;
    }

    public Vector2f getVisualCoordinate(Vector2f logicalCoordinate) {
        return getVisualCoordinate(logicalCoordinate.x, logicalCoordinate.y);
    }

    public Vector2f getVisualCoordinate(float x, float y) {
        Vector2f visualCoordinate = new Vector2f();
        //Checks wether I want to rotate it for the black player or not
        x = (isBlackPlayer()) ? getTilesX() - 1 - x : x;
        y = (isBlackPlayer()) ? getTilesY() - 1 - y : y;
        visualCoordinate.x = getBorderSize() + x * getTileSize();
        visualCoordinate.y = getBorderSize() + y * getTileSize();
        visualCoordinate.multLocal(getScreenScaling());
        return visualCoordinate;
    }

    /**
     * @return the tileSize
     */
    public int getTileSize() {
        return tileSize;
    }

    /**
     * @param tileSize the tileSize to set
     */
    public void setTileSize(int tileSize) {
        this.tileSize = tileSize;
    }

    /**
     * @return the borderSize
     */
    public int getBorderSize() {
        return borderSize;
    }

    /**
     * @param borderSize the borderSize to set
     */
    public void setBorderSize(int borderSize) {
        this.borderSize = borderSize;
    }

    /**
     * @return the blackPlayer
     */
    public boolean isBlackPlayer() {
        return blackPlayer;
    }

    /**
     * @param blackPlayer the blackPlayer to set
     */
    public void setBlackPlayer(boolean blackPlayer) {
        this.blackPlayer = blackPlayer;
    }

    /**
     * @return the tiles
     */
    public int getTilesX() {
        return tilesX;
    }

    /**
     * @param tiles the tiles to set
     */
    public void setTilesX(int tiles) {
        this.tilesX = tiles;
    }

    /**
     * @return the tiles
     */
    public int getTilesY() {
        return tilesY;
    }

    /**
     * @param tiles the tiles to set
     */
    public void setTilesY(int tiles) {
        this.tilesY = tiles;
    }

    /**
     * @return the screenSize
     */
    public Vector2f getScreenSize() {
        return screenSize;
    }

    /**
     *
     * @return
     */
    private float getScreenScaling() {
        Vector2f scaling = new Vector2f();
        scaling.x = getScreenSize().x / getLogicalScreenSize().x;
        scaling.y = getScreenSize().y / getLogicalScreenSize().y;
        if (scaling.x < scaling.y) {
            return scaling.x;
        }
        return scaling.y;
    }

    /**
     * Sets the screen size for scaling, make sure to feed the smallest screen
     * size value for tiles to stay inside screen.
     *
     * @param screenSize the screenSize to set
     */
    public void setScreenSize(Vector2f screenSize) {
        this.screenSize = screenSize;
    }

    /**
     * screenSize.x = getTileSize() * getTilesX() + getBorderSize() * 2;
     * screenSize.y = getTileSize() * getTilesY() + getBorderSize() * 2;
     *
     * @param width
     * @param height
     */
    public void setScreenSize(int width, int height) {
        setScreenSize(new Vector2f(width, height));
    }

    /**
     * @return the logicalScreenSize
     */
    private Vector2f getLogicalScreenSize() {
        return logicalScreenSize;
    }
}
