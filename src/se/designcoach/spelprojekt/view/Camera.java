/*
 *
 * Copyright 2013 Design Coach Sweden.
 */
package se.designcoach.spelprojekt.view;

import com.jme3.math.Vector2f;

/**
 *
 * @author lohnn
 */
public class Camera {

    private Vector2f boardSize = new Vector2f(1, 1);

    public Vector2f getWorldCoordinate(Vector2f logicalCoordinate) {
        float x = boardSize.x * logicalCoordinate.x;
        float y = boardSize.y * logicalCoordinate.y;
        return new Vector2f(x, y);
    }

    /**
     * @return the boardSize
     */
    public Vector2f getBoardSize() {
        return boardSize;
    }

    /**
     * @param boardSize the boardSize to set
     */
    public void setBoardSize(Vector2f boardSize) {
        this.boardSize = boardSize;
    }

    public void setBoardSize(float width, float height) {
        this.setBoardSize(new Vector2f(width, height));
    }
}
