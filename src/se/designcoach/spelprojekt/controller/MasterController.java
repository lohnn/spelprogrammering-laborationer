/*
 *
 * Copyright 2013 Design Coach Sweden.
 */
package se.designcoach.spelprojekt.controller;

import com.jme3.app.SimpleApplication;
import com.jme3.renderer.RenderManager;
import se.designcoach.spelprojekt.model.BallSimulation;
import se.designcoach.spelprojekt.view.BallView;
import se.designcoach.spelprojekt.view.ChessView;

/**
 *
 * @author lohnn
 */
public class MasterController extends SimpleApplication {

    BallSimulation ballSim;
    BallView ballView;

    @Override
    public void simpleInitApp() {
        cam.setParallelProjection(true);
        cam.setFrustum(-50, 500, -10, 10, -10, 10);
        ChessView cv = new ChessView();
        System.out.println(cv.getVisualCoordinate(0, 0));
        System.out.println(cv.getVisualCoordinate(7, 0));
        System.out.println(cv.getVisualCoordinate(1, 7));
        System.out.println(cv.getVisualCoordinate(7, 7));
        cv.setBlackPlayer(true);
        System.out.println("----------------");
        System.out.println(cv.getVisualCoordinate(0, 0));
        System.out.println(cv.getVisualCoordinate(6, 0));
        System.out.println(cv.getVisualCoordinate(2, 7));
        System.out.println(cv.getVisualCoordinate(7, 7));
        cv.setScreenSize(320, 240);
        cv.setBlackPlayer(false);
        System.out.println("----------------");
        System.out.println(cv.getVisualCoordinate(0, 0));
        System.out.println(cv.getVisualCoordinate(7, 0));
        System.out.println(cv.getVisualCoordinate(1, 7));
        System.out.println(cv.getVisualCoordinate(7, 7));

        ballSim = new BallSimulation();
        ballView = new BallView(this, ballSim);
    }

    @Override
    public void simpleUpdate(float tpf) {
        ballView.update(tpf);
    }

    @Override
    public void simpleRender(RenderManager rm) {
    }
}
