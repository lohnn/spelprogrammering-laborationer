/*
 *
 * Copyright 2013 Design Coach Sweden.
 */
package se.designcoach.spelprojekt;

import se.designcoach.spelprojekt.controller.MasterController;

/**
 * @author Lohnn
 */
public class Main {

    public static void main(String[] args) {
        MasterController controller = new MasterController();
        controller.start();
    }
}
