/*
 *
 * Copyright 2013 Design Coach Sweden.
 */
package se.designcoach.spelprojekt.model;

import com.jme3.math.Vector2f;

/**
 *
 * @author lohnn
 */
public class Ball {

    private float radius,
            speed;
    private Vector2f position = new Vector2f(),
            direction = new Vector2f();

    public Ball(float radius) {
        init(radius);
    }

    public Ball() {
        this(1f);
    }

    private void init(float radius) {
        setRadius(radius);
        setSpeed(1f);
        setDirection(1, 0);
    }

    /**
     * @return the speed
     */
    public float getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(float speed) {
        this.speed = speed;
    }

    /**
     * @return the radius
     */
    public float getRadius() {
        return radius;
    }

    /**
     * @param radius the radius to set
     */
    public void setRadius(float radius) {
        this.radius = radius;
    }

    /**
     * @return the position
     */
    public Vector2f getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(Vector2f position) {
        this.position = position;
    }

    /**
     * @return the direction
     */
    public Vector2f getDirection() {
        return direction;
    }

    /**
     * @param direction the direction to set
     */
    public void setDirection(Vector2f direction) {
        this.setDirection(direction.x, direction.y);
    }

    public void setDirection(float x, float y) {
        this.direction.set(x, y);
    }
}
