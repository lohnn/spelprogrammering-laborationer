/*
 *
 * Copyright 2013 Design Coach Sweden.
 */
package se.designcoach.spelprojekt.model;

import com.jme3.math.Vector2f;

/**
 *
 * @author lohnn
 */
public class BallSimulation {

    private Ball ball = new Ball(.3f);
    private Vector2f boundaries = new Vector2f(1, 1);

    public BallSimulation() {
        ball.setSpeed(.5f);
        ball.setDirection(1, .8f);
    }

    public void update(float tpf) {
        move(tpf);
    }

    private void move(float tpf) {
        float howMuchToMove = ball.getSpeed() * tpf;
        Vector2f direction = ball.getDirection().normalize();
        direction = direction.mult(howMuchToMove);
        ball.setPosition(ball.getPosition().add(direction));
        collide();
    }

    private void collide() {
        if (ball.getPosition().x >= boundaries.x
                || ball.getPosition().x <= 0) {
            ball.setDirection(ball.getDirection().setX(-ball.getDirection().x));
        }
        if (ball.getPosition().y >= boundaries.y
                || ball.getPosition().y <= 0) {
            ball.setDirection(ball.getDirection().setY(-ball.getDirection().y));
        }
    }

    public Ball getBall() {
        return ball;
    }
}
